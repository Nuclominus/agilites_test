package com.nuclominus.agilites.Abstract;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.nuclominus.agilites.R;
import com.nuclominus.agilites.Util.FontsUtil;

public abstract class ParentActivity extends AppCompatActivity {

    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();

        if (findViewById(R.id.toolbar) != null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        FontsUtil.overrideFonts(this, getWindow().getDecorView().getRootView());
        showLeftAnimation(this);
    }

    protected void setToolbarView(int id){
        ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.toolbar_content);
        inclusionViewGroup.addView(LayoutInflater.from(this).inflate(id, null));
    }
    protected void initUI() {
    }

    public void loadProcess(boolean visibility) {
        if (findViewById(R.id.pB_load) != null) {
            int visible = visibility ? View.VISIBLE : View.GONE;
            findViewById(R.id.pB_load).setVisibility(visible);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        showRightAnimation(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result;

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                result = true;
                break;
            default:
                result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    protected void setBackground(int background) {
        if (findViewById(R.id.iv_bg) != null)
            ((ImageView) findViewById(R.id.iv_bg)).setImageBitmap(BitmapFactory.decodeResource(getResources(), background));
    }

    protected static void showLeftAnimation(final Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    protected static void showRightAnimation(final Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }
}
