package com.nuclominus.agilites.Network;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.nuclominus.agilites.Abstract.ParentActivity;
import com.nuclominus.agilites.BuildConfig;
import com.nuclominus.agilites.Object.Task;
import com.nuclominus.agilites.Object.UserData;
import com.nuclominus.agilites.Util.Const;
import com.nuclominus.agilites.Util.PreferencesManager;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class APIFactory {

    public interface APIFactoryCallbackInterface {
        void callbackSignin(UserData user);
        void callbackTaskList(boolean status, JSONObject answer);
    }

    private static AQuery aq;
    private static APIFactory instance = null;
    private static Context context;

    public static synchronized void initializeInstance(Context context) {
        if (instance == null) {
            instance = new APIFactory();
            instance.context = context;
            instance.aq = new AQuery(context);
        }
    }

    public static synchronized APIFactory getInstance() {
        if (instance == null) {
            throw new IllegalStateException(APIFactory.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return instance;
    }

    private APIFactory() {
    }

    public void sign(SignRequestObject signRequestObject, AjaxCallback<JSONObject> callback) {
        aq.post(BuildConfig.API_URL + "Account/Login", signRequestObject.getParams(), JSONObject.class, callback);
    }

    public void tasks_list(TaskListRequestObject taskListRequestObject, AjaxCallback<JSONObject> callback) {
        callback.header(Const.AUTH_TOKEN, PreferencesManager.getInstance().getStringValue(Const.AUTH_TOKEN))
                .header(Const.SESSION_TOKEN, PreferencesManager.getInstance().getStringValue(Const.SESSION_TOKEN));
        aq.ajax(taskListRequestObject.getParams(BuildConfig.API_URL + "Patients/CoachingPlan/PatientPlan"), JSONObject.class, callback);
    }

    public void signIn(ParentActivity activity, final APIFactoryCallbackInterface callbackInterface) {
        activity.loadProcess(true);
        APIFactory.getInstance().sign(
                new SignRequestObject(BuildConfig.TEST_LOGIN,
                        BuildConfig.TEST_PASSWORD), new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject answer, AjaxStatus status) {
                        super.callback(url, answer, status);
                        if (answer != null) {
                            callbackInterface.callbackSignin(UserData.parseUser(answer));
                        } else {
                            callbackInterface.callbackSignin(null);
                        }
                    }
                });
    }

    public void getTaskList(final UserData user, final APIFactoryCallbackInterface callbackInterface) {
        TaskListRequestObject requestBody = new TaskListRequestObject(
                user.getGroupPlanID(),
                user.getUserPlanPhaseID(),
                user.getDate(System.currentTimeMillis(), Const.TIME_FORMAT_TASK_LIST)
        );

        APIFactory.getInstance().tasks_list(requestBody, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject answer, AjaxStatus status) {
                super.callback(url, answer, status);
                if (answer != null) {
                    callbackInterface.callbackTaskList(true, answer);
                } else {
                    callbackInterface.callbackTaskList(false, null);
                }
            }
        });
    }

    public void reportItem(Task task, AjaxCallback<String> callback) {
        Map<String, Object> params = new HashMap<>();
        callback.header(Const.AUTH_TOKEN, PreferencesManager.getInstance().getStringValue(Const.AUTH_TOKEN))
                .header(Const.SESSION_TOKEN, PreferencesManager.getInstance().getStringValue(Const.SESSION_TOKEN))
                .method(AQuery.METHOD_PUT);
        aq.ajax(task.getParams(BuildConfig.API_URL + "Patients/CoachingPlan/ReportItem"),params, String.class, callback);
    }

}