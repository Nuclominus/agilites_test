package com.nuclominus.agilites.Network;

import com.nuclominus.agilites.Util.Const;

import org.apache.http.HttpEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TaskListRequestObject {
    private String planID;
    private String phaseID;
    private String reportDate;

    public TaskListRequestObject(String planID, String phaseID, String reportDate) {
        this.planID = planID;
        this.phaseID = phaseID;
        this.reportDate = reportDate;
    }

    public String getPlanID() {
        return planID;
    }

    public String getPhaseID() {
        return phaseID;
    }

    public String getReportDate() {
        return reportDate;
    }

    public String getParams(String base_url) {
        return new StringBuilder(base_url)
                .append("?")
                .append(Const.USER_PARAM_PLAIN_ID + getPlanID() + "&")
                .append(Const.USER_PARAM_PHASE_ID + getPhaseID() + "&")
                .append(Const.USER_PARAM_REPORT_DATE + getReportDate()).toString();
    }
}
