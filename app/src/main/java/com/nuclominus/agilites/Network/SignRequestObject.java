package com.nuclominus.agilites.Network;

import com.nuclominus.agilites.Util.Const;

import org.json.JSONException;
import org.json.JSONObject;

public class SignRequestObject {
    private String UserName;
    private String Password;

    public SignRequestObject(String userName, String password) {
        UserName = userName;
        Password = password;
    }

    public String getUserName() {
        return UserName;
    }

    public String getPassword() {
        return Password;
    }

    public JSONObject getParams(){
        try {
            return new JSONObject().put(Const.USER_NAME, getUserName()).put(Const.USER_PASSWORD, getPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
