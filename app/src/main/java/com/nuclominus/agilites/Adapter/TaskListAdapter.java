package com.nuclominus.agilites.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.nuclominus.agilites.Network.APIFactory;
import com.nuclominus.agilites.Object.Task;
import com.nuclominus.agilites.R;
import com.nuclominus.agilites.Util.AnimationUtil;

import org.json.JSONObject;

import java.util.ArrayList;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.ViewHolder> {

    private ArrayList<Task> taskArrayList = null;
    private Context context;

    public TaskListAdapter(ArrayList<Task> taskArrayList, Context context) {
        this.taskArrayList = taskArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Task task = taskArrayList.get(position);
        holder.title.setText(task.getTitle());
        holder.motto.setText(task.getSubTitle());
        holder.imageView.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!task.getReported()) {
                    sendCheckItem(task, holder);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (taskArrayList != null)
            return taskArrayList.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView title;
        protected TextView motto;
        protected ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            title = ((TextView) itemView.findViewById(R.id.tV_tasklist_item_title));
            motto = ((TextView) itemView.findViewById(R.id.tV_tasklist_item_motto));
            imageView = ((ImageView) itemView.findViewById(R.id.iV_tasklist_item_reported));
        }
    }

    private void sendCheckItem(final Task task, final ViewHolder holder) {
        APIFactory.getInstance().reportItem(task, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String answer, AjaxStatus status) {
                super.callback(url, answer, status);
                if (answer != null) {
                    if (Boolean.parseBoolean(answer) && !task.getReported()) {
                        AnimationUtil.transitionImageSmile(context, holder.imageView);
                        task.setReported(true);
                    }
                }
            }
        });
    }
}
