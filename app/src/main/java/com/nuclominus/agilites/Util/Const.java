package com.nuclominus.agilites.Util;

public class Const {
    public static final String TIME_FORMAT_TASK_LIST = "yyyy-MM-dd";

    public static final String TOKENS = "Tokens";
    public static final String AUTH_TOKEN = "AuthToken";
    public static final String SESSION_TOKEN = "SessionToken";


    public static final String USER = "User";
    public static final String USER_NAME = "UserName";
    public static final String USER_PASSWORD = "Password";

    public static final String USER_PARAM_PLAIN_ID = "planID=";
    public static final String USER_PARAM_PHASE_ID = "phaseID=";
    public static final String USER_PARAM_REPORT_DATE = "reportDate=";
    public static final String PHASE_TASK_PLAN_ITEM_ID = "planItemID=";

    public static final String USER_GROUP_ID = "GroupID";
    public static final String USER_GROUP_PLAN_ID = "GroupPlanID";
    public static final String USER_PLAN_PHASE_ID = "UserPlanPhaseID";
    public static final String USER_PHOTO_URL = "PhotoURL";
    public static final String USER_FULL_NAME = "FullName";

    public static final String PHASE_TASK_PHASE_ID = "PhaseID";
    public static final String PHASE_TASK_PHASE_NAME = "PhaseName";
    public static final String PHASE_TASK_PHASE_MOTTO = "PhaseMotto";
    public static final String PHASE_TASK_PHASE_TASKS = "PhaseTasks";

    public static final String TITLE = "Title";
    public static final String SUBTITLE = "SubTitle";
    public static final String REPORTED = "Reported";
    public static final String INSERTDATE = "InsertDate";
    public static final String ORIGINAL_PHASE_TASK_ID = "OriginalPhaseTaskID";

}
