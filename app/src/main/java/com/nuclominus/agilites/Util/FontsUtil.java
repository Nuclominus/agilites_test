package com.nuclominus.agilites.Util;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nuclominus.agilites.R;

public class FontsUtil {

    private static FontsUtil instance;

    private static Typeface latoBold;


    public static void initializeInstance(Context context){
        if(instance == null) {
            instance = new FontsUtil();
        }
        instance.initFontStyle(context);
    }

    private FontsUtil() {}

    public static synchronized FontsUtil getInstance() {
        if (instance == null) {
            throw new IllegalStateException(AppAgilites.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return instance;
    }

    public void initFontStyle(Context context){
        latoBold = Typeface.createFromAsset(context.getAssets(),
                context.getString(R.string.font_lato_bold));
    }

    public static void overrideFonts(Context context , final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof EditText) {
                ((EditText) v).setTypeface(FontsUtil.getLatoBold());
            } else if (v instanceof Button) {
                ((Button) v).setTypeface(FontsUtil.getLatoBold());
            }
        } catch (Exception e) {
        }
    }

    public static Typeface getLatoBold() {
        return latoBold;
    }

}
