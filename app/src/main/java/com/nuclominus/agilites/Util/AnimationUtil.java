package com.nuclominus.agilites.Util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.TransitionDrawable;
import android.widget.ImageView;

import com.nuclominus.agilites.R;

public class AnimationUtil {

    public static void transitionImageSmile(Context context, ImageView imageView){
        Resources res = context.getApplicationContext().getResources();
        TransitionDrawable transition = (TransitionDrawable) res.getDrawable(R.drawable.image_task_item_transition);
        imageView.setImageDrawable(transition);
        transition.startTransition(300);
    }
}
