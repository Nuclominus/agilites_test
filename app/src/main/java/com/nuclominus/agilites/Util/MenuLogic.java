package com.nuclominus.agilites.Util;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nuclominus.agilites.R;

public class MenuLogic implements View.OnClickListener {

    public interface OnMenuItemClick {
        void OnClick(View v, int position);
    }

    RelativeLayout rlMenuHome, rlMenuMan,
            rlMenuPeople, rlMenuMail, rlMenuOther;
    ImageView iV_Menu_home, iV_Menu_man, iV_Menu_people,
            iV_Menu_mail, iV_Menu_other;
    Context context;
    OnMenuItemClick clickInterface;

    public MenuLogic(AppCompatActivity context, OnMenuItemClick clickInterface) {

        this.context = context;
        this.clickInterface = clickInterface;

        // layouts
        this.rlMenuHome = (RelativeLayout) context.findViewById(R.id.rlMenuHome);
        this.rlMenuMan = (RelativeLayout) context.findViewById(R.id.rlMenuMan);
        this.rlMenuPeople = (RelativeLayout) context.findViewById(R.id.rlMenuPeople);
        this.rlMenuMail = (RelativeLayout) context.findViewById(R.id.rlMenuMail);
        this.rlMenuOther = (RelativeLayout) context.findViewById(R.id.rlMenuOther);

        rlMenuHome.setOnClickListener(this);
        rlMenuMan.setOnClickListener(this);
        rlMenuPeople.setOnClickListener(this);
        rlMenuMail.setOnClickListener(this);
        rlMenuOther.setOnClickListener(this);

        // images
        this.iV_Menu_home = (ImageView) context.findViewById(R.id.iV_Menu_home);
        this.iV_Menu_man = (ImageView) context.findViewById(R.id.iV_Menu_man);
        this.iV_Menu_people = (ImageView) context.findViewById(R.id.iV_Menu_people);
        this.iV_Menu_mail = (ImageView) context.findViewById(R.id.iV_Menu_mail);
        this.iV_Menu_other = (ImageView) context.findViewById(R.id.iV_Menu_other);

        deselect();
    }

    public void setCountEmail(int count){
    }

    public void deselect() {
        iV_Menu_home.setImageResource(R.drawable.ic_home_normal);
        iV_Menu_man.setImageResource(R.drawable.ic_directions_run_normal);
        iV_Menu_people.setImageResource(R.drawable.ic_people_normal);
        iV_Menu_mail.setImageResource(R.drawable.ic_email_normal);
        iV_Menu_other.setImageResource(R.drawable.ic_other_normal);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlMenuHome: {
                deselect();
                iV_Menu_home.setImageResource(R.drawable.ic_home_select);
                clickInterface.OnClick(iV_Menu_home, 1);
            }
            break;

            case R.id.rlMenuMan: {
                deselect();
                iV_Menu_man.setImageResource(R.drawable.ic_directions_run_select);
                clickInterface.OnClick(iV_Menu_man, 2);
            }
            break;

            case R.id.rlMenuPeople: {
                deselect();
                iV_Menu_people.setImageResource(R.drawable.ic_people_select);
                clickInterface.OnClick(iV_Menu_people, 3);
            }
            break;

            case R.id.rlMenuMail: {
                deselect();
                iV_Menu_mail.setImageResource(R.drawable.ic_email_select);
                clickInterface.OnClick(iV_Menu_mail, 4);
            }
            break;

            case R.id.rlMenuOther: {
                deselect();
                iV_Menu_other.setImageResource(R.drawable.ic_other_select);
                clickInterface.OnClick(iV_Menu_other, 5);
            }
            break;
        }
    }
}
