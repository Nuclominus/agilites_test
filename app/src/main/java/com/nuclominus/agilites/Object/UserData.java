package com.nuclominus.agilites.Object;

import android.os.Parcel;
import android.os.Parcelable;

import com.nuclominus.agilites.Util.Const;
import com.nuclominus.agilites.Util.PreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UserData implements Parcelable{
    private String GroupID;
    private String GroupPlanID;
    private String UserPlanPhaseID;
    private String PhotoURL;
    private String FullName;
    private JSONObject phaseTask;
    private JSONObject data;

    public UserData(String groupID, String groupPlanID, String userPlanPhaseID, String photoURL, String fullName) {
        GroupID = groupID;
        GroupPlanID = groupPlanID;
        UserPlanPhaseID = userPlanPhaseID;
        PhotoURL = photoURL;
        FullName = fullName;
    }

    public String getGroupID() {
        return GroupID;
    }

    public String getGroupPlanID() {
        return GroupPlanID;
    }

    public String getUserPlanPhaseID() {
        return UserPlanPhaseID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public String getFullName() {
        return FullName;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public static UserData parseUser(JSONObject answer) {
        UserData userData = null;
        try {
            JSONObject user = answer.getJSONObject(Const.USER);
            userData = new UserData(
                    user.getString(Const.USER_GROUP_ID),
                    user.getString(Const.USER_GROUP_PLAN_ID),
                    user.getString(Const.USER_PLAN_PHASE_ID),
                    user.getString(Const.USER_PHOTO_URL),
                    user.getString(Const.USER_FULL_NAME));
            userData.setData(answer);
            userData.parseTokens(answer.getJSONObject(Const.TOKENS));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userData;
    }

    private void parseTokens(JSONObject token) {
        if (token != null) {
            try {
                PreferencesManager.getInstance().setStringValue(Const.AUTH_TOKEN, token.getString(Const.AUTH_TOKEN));
                PreferencesManager.getInstance().setStringValue(Const.SESSION_TOKEN, token.getString(Const.SESSION_TOKEN));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getDate(long milliseconds, String format) {
        Date date = new Date(milliseconds);
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        String formattedDate = sdf.format(date);
        return String.valueOf(formattedDate);
    }

    public JSONObject getPhaseTask() {
        return phaseTask;
    }

    public void setPhaseTask(JSONObject phaseTask) {
        this.phaseTask = phaseTask;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getGroupID());
        dest.writeString(getGroupPlanID());
        dest.writeString(getUserPlanPhaseID());
        dest.writeString(getPhotoURL());
        dest.writeString(getFullName());
        dest.writeString(getPhaseTask().toString());
        dest.writeString(getData().toString());
    }

    public static final Creator<UserData> CREATOR = new Creator<UserData>() {
        @Override
        public UserData createFromParcel(Parcel in) {
            return new UserData(in);
        }

        @Override
        public UserData[] newArray(int size) {
            return new UserData[size];
        }
    };

    protected UserData(Parcel in) {
        GroupID = in.readString();
        GroupPlanID = in.readString();
        UserPlanPhaseID = in.readString();
        PhotoURL = in.readString();
        FullName = in.readString();

        try {
            phaseTask = new JSONObject(in.readString());
            data = new JSONObject(in.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
