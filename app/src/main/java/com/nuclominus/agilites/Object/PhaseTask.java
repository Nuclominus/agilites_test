package com.nuclominus.agilites.Object;


import android.os.Parcel;
import android.os.Parcelable;

import com.nuclominus.agilites.Util.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PhaseTask implements Parcelable{
    private String phaseID;
    private String phaseName;
    private String phaseMotto;
    private ArrayList<Task> taskArrayList;
    private JSONObject data;

    public PhaseTask(String phaseID, String phaseName, String phaseMotto) {
        this.phaseID = phaseID;
        this.phaseName = phaseName;
        this.phaseMotto = phaseMotto;
    }

    public String getPhaseID() {
        return phaseID;
    }

    public String getPhaseName() {
        return phaseName;
    }

    public String getPhaseMotto() {
        return phaseMotto;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public ArrayList<Task> getTaskArrayList() {
        return taskArrayList;
    }

    public static PhaseTask parsePhaseTask(JSONObject data) {
        PhaseTask phaseTask = null;
        try {
            phaseTask = new PhaseTask(
                    data.getString(Const.PHASE_TASK_PHASE_ID),
                    data.getString(Const.PHASE_TASK_PHASE_NAME),
                    data.getString(Const.PHASE_TASK_PHASE_MOTTO)
            );
            phaseTask.setData(data);
            phaseTask.parseTasks(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return phaseTask;
    }

    private void parseTasks(JSONObject data) {
        JSONArray taskJsonArray = null;
        try {
            taskJsonArray = data.getJSONArray(Const.PHASE_TASK_PHASE_TASKS);
            if (taskJsonArray != null && taskJsonArray.length() > 0)
                for (int i = 0; i < taskJsonArray.length(); i++) {
                    if (taskArrayList == null)
                        taskArrayList = new ArrayList<>();
                    taskArrayList.add(new Task(
                            taskJsonArray.getJSONObject(i).getString(Const.TITLE),
                            taskJsonArray.getJSONObject(i).getString(Const.SUBTITLE),
                            taskJsonArray.getJSONObject(i).getBoolean(Const.REPORTED),
                            taskJsonArray.getJSONObject(i).getString(Const.INSERTDATE),
                            taskJsonArray.getJSONObject(i).getString(Const.ORIGINAL_PHASE_TASK_ID)
                    ));
                }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getPhaseID());
        dest.writeString(getPhaseName());
        dest.writeString(getPhaseMotto());
        dest.writeString(getData().toString());
    }

    protected PhaseTask(Parcel in) {

        phaseID = in.readString();
        phaseName = in.readString();
        phaseMotto = in.readString();

        try {
            data = new JSONObject(in.readString());
            parseTasks(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static final Creator<PhaseTask> CREATOR = new Creator<PhaseTask>() {
        @Override
        public PhaseTask createFromParcel(Parcel in) {
            return new PhaseTask(in);
        }

        @Override
        public PhaseTask[] newArray(int size) {
            return new PhaseTask[size];
        }
    };
}
