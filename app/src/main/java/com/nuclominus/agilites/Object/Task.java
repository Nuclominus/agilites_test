package com.nuclominus.agilites.Object;

import com.nuclominus.agilites.Util.Const;

public class Task {
    private String title;
    private String subTitle;
    private Boolean reported;
    private String insertDate;
    private String OriginalPhaseTaskID;

    public Task(String title, String subTitle, Boolean reported, String insertDate, String OriginalPhaseTaskID) {
        this.title = title;
        this.subTitle = subTitle;
        this.reported = reported;
        this.insertDate = insertDate;
        this.OriginalPhaseTaskID = OriginalPhaseTaskID;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public Boolean getReported() {
        return reported;
    }

    public void setReported(Boolean reported) {
        this.reported = reported;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public String getOriginalPhaseTaskID() {
        return OriginalPhaseTaskID;
    }

    public String getParams(String base_url){
        return new StringBuilder(base_url)
                .append("?")
                .append(Const.PHASE_TASK_PLAN_ITEM_ID + getOriginalPhaseTaskID() + "&")
                .append(Const.USER_PARAM_REPORT_DATE + UserData.getDate(System.currentTimeMillis(), Const.TIME_FORMAT_TASK_LIST)).toString();
    }
}
