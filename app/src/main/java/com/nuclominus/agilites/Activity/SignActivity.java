package com.nuclominus.agilites.Activity;

import android.content.Intent;
import android.view.View;

import com.nuclominus.agilites.Abstract.ParentActivity;
import com.nuclominus.agilites.Network.APIFactory;
import com.nuclominus.agilites.Object.UserData;
import com.nuclominus.agilites.R;

import org.json.JSONObject;

public class SignActivity extends ParentActivity implements APIFactory.APIFactoryCallbackInterface{

    private UserData user;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_sign);
        setBackground(R.drawable.bg);

        findViewById(R.id.btn_signin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIFactory.getInstance().signIn(SignActivity.this, SignActivity.this);
            }
        });
    }

    @Override
    public void callbackSignin(UserData user) {
        this.user = user;
        APIFactory.getInstance().getTaskList(user, this);
    }

    @Override
    public void callbackTaskList(boolean status, JSONObject answer) {
        if(status) {
            loadProcess(false);
            user.setPhaseTask(answer);
            startActivity(new Intent(SignActivity.this, TaskListActivity.class)
                    .putExtra(UserData.class.getCanonicalName(), user));
        }
    }
}
