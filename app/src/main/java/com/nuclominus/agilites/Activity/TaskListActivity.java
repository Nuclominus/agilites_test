package com.nuclominus.agilites.Activity;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.nuclominus.agilites.Abstract.ParentActivity;
import com.nuclominus.agilites.Adapter.TaskListAdapter;
import com.nuclominus.agilites.Network.APIFactory;
import com.nuclominus.agilites.Object.PhaseTask;
import com.nuclominus.agilites.Object.Task;
import com.nuclominus.agilites.Object.UserData;
import com.nuclominus.agilites.R;
import com.nuclominus.agilites.Util.MenuLogic;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TaskListActivity extends ParentActivity implements MenuLogic.OnMenuItemClick, APIFactory.APIFactoryCallbackInterface {

    private UserData userData;
    private PhaseTask phaseTask;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;

    private ArrayList<Task> taskArrayList = new ArrayList<>();
    private TaskListAdapter adapter;
    private SwipeRefreshLayout refreshView;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_task_list);

        userData = getIntent().getParcelableExtra(
                UserData.class.getCanonicalName());

        initToolbar();
        initRecycler();
        initMenu();
        initAdapter();

    }

    private void initMenu() {
        MenuLogic menuLogic = new MenuLogic(this, this);
        menuLogic.setCountEmail(12);
    }

    private void initToolbar() {
        setToolbarView(R.layout.tasklist_toolbar_content);

        phaseTask = PhaseTask.parsePhaseTask(userData.getPhaseTask());
        taskArrayList.clear();
        taskArrayList.addAll(phaseTask.getTaskArrayList());

        ((TextView) findViewById(R.id.tV_TaskList_Title)).setText(userData.getFullName() + getString(R.string.task_toolbar_title_prefix));
        ((TextView) findViewById(R.id.tVTaskList_Content)).setText(phaseTask.getPhaseName() + ": " + phaseTask.getPhaseMotto());

        loadProcess(true);
        Glide.with(this)
                .load(userData.getPhotoURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        loadProcess(false);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        loadProcess(false);
                        return false;
                    }
                })
                .into((ImageView) findViewById(R.id.iV_TaskList_Profile));
    }

    private void initRecycler() {
        recyclerView = (RecyclerView) findViewById(R.id.rV_TaskList);
        refreshView = (SwipeRefreshLayout) findViewById(R.id.refreshView);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                refreshView.setEnabled(topRowVerticalPosition >= 0);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                APIFactory.getInstance().getTaskList(userData, TaskListActivity.this);
            }
        });

        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(adapter);
    }

    private void initAdapter() {
        refreshView.setRefreshing(false);
        adapter = null;
        if (taskArrayList != null) {
            if (taskArrayList.size() > 0)
                Collections.sort(taskArrayList, new Comparator<Task>() {
                    @Override
                    public int compare(Task lhs, Task rhs) {
                        return lhs.getTitle().compareTo(rhs.getTitle());
                    }
                });
            adapter = new TaskListAdapter(taskArrayList, this);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void OnClick(View v, int position) {
        if (position == 5) {
            // floating button
            findViewById(R.id.multiple_actions).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.multiple_actions).setVisibility(View.GONE);
        }
    }

    @Override
    public void callbackSignin(UserData user) {
        // no answer
        return;
    }

    @Override
    public void callbackTaskList(boolean status, JSONObject answer) {
        refreshView.setRefreshing(false);
        userData.setPhaseTask(answer);
        initUI();
    }
}
